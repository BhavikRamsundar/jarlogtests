﻿using System;
using System.Net;
using System.Threading.Tasks;
using JarLogTests.Builders;
using JarLogTests.Models;
using JarLogTests.Repositories;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Linq;

namespace JarLogTests
{
    [TestFixture]
    public class JarLogTests
    {
        private readonly JarLogApiRepository _jarLogApiRepository;

        public JarLogTests()
        {
            _jarLogApiRepository = new JarLogApiRepository(new Uri("https://localhost:9351"));
        }

        [Test]
        public async Task Post_GivenJar_ShouldCreateJar()
        {
            //given
            var expectedJar = new JarBuilder().Build();

            //execute
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Id, Is.EqualTo(expectedJar.Id));
        }

        [Test]
        public async Task Post_GivenJarAlreadyExists_ShouldUpdateJar()
        {
            var expectedJar = new JarBuilder().Build();
            var postResponse = await _jarLogApiRepository.Post(expectedJar);
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            expectedJar.Note = "I updated!";

            var post2Response = await _jarLogApiRepository.Post(expectedJar);

            Assert.That(post2Response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Note, Is.EqualTo(expectedJar.Note));
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task Post_GivenJarWithNullEmptyOrWhitespaceId_ShouldReturnBadRequest(string id)
        {
            var expectedJar = new JarBuilder().WithId(id).Build();

            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            var message = await postResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo("The JarId cannot be null, empty or whitespace."));
        }

        [TestCase("balaalksfhaslk")]
        [TestCase("testies")]
        [TestCase("boos")]
        public async Task Get_GivenInvalidJarId_ShouldReturnBadRequest(string id)
        {
            var expectedJar = new JarBuilder().WithId(id).Build();
            var getResponse = await _jarLogApiRepository.Post(expectedJar);

            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            var message = await getResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo("The JarId is invalid. A JarId can only be a GUID."));
        }

        [Test]
        public async Task Get_GivenJarIdAlreadyExists_ShouldReturnJar()
        {
            var expectedJar = new JarBuilder().Build();
            var postResponse = await _jarLogApiRepository.Post(expectedJar);
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);

            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Id, Is.EqualTo(expectedJar.Id));
        }

        [Test]
        public async Task Get_GivenJarIdDoesNotExist_ShouldReturnNotFound()
        {
            var id = Guid.NewGuid().ToString();

            var getResponse = await _jarLogApiRepository.Get(id);

            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            var message = await getResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo($"Could not find jar with Id: {id}."));
        }

        [Test]
        public async Task Delete_GivenJarIdAlreadyExists_ShouldRemoveJar()
        {
            var expectedJar = new JarBuilder().Build();
            var postResponse = await _jarLogApiRepository.Post(expectedJar);
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            var deleteResult = await _jarLogApiRepository.Delete(expectedJar.Id);

            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            var message = await getResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo($"Could not find jar with Id: {expectedJar.Id}."));
        }


        [Test]
        public async Task Delete_GivenJarIdDoesNotExist_ShouldReturnNotFound()
        {
            var id = Guid.NewGuid().ToString();

            var deleteResponse = await _jarLogApiRepository.Delete(id);

            Assert.That(deleteResponse.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            var message = await deleteResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo($"Could not find jar with Id: {id}."));
        }

        [TestCase("sdeffwef")]
        [TestCase("ewf")]
        [TestCase("sdsdsd")]
        public async Task Post_GivenInvalidJarId_ShouldReturnBadRequest(string id)
        {
            var expectedJar = new JarBuilder().WithId(id).Build();

            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            var message = await postResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo($"The JarId is invalid. A JarId can only be a GUID."));
        }

        [TestCase("sdeffwef")]
        [TestCase("ewf")]
        [TestCase("sdsdsd")]
        public async Task Delete_GivenInvalidJarId_ShouldReturnBadRequest(string id)
        {
            var expectedJar = new JarBuilder().WithId(id).Build();

            var deleteResponse = await _jarLogApiRepository.Delete(expectedJar.Id);

            Assert.That(deleteResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            var message = await deleteResponse.Content.ReadAsStringAsync();
            Assert.That(message, Is.EqualTo("The JarId is invalid. A JarId can only be a GUID."));
        }

        [Test]
        public async Task Post_GivenJarWithPastdate_ShouldCreateJar()
        {
            //given
            var pastDate = DateTime.Now.AddDays(-1);
            var expectedJar = new JarBuilder().WithDate(pastDate).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Date, Is.EqualTo(pastDate));
        }

        [Test]
        public async Task Post_GivenJarFutureDate_ShouldCreateJar()
        {
            //given
            var futureDate = DateTime.Now.AddMonths(1);
            var expectedJar = new JarBuilder().WithDate(futureDate).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Date, Is.EqualTo(futureDate));
        }

        [Test]
        public async Task Post_GivenJarWithPresetDate_ShouldCreateJar()
        {
            //given
            var presentDate = DateTime.Now;
            var expectedJar = new JarBuilder().WithDate(presentDate).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Date, Is.EqualTo(presentDate));
        }

        [Test]
        public async Task Post_GivenJarWithLeapDate_ShouldCreateJar()
        {
            //given
            var leapDate = new DateTime(2020, 1, 1, 1, 1, 1);
            var expectedJar = new JarBuilder().WithDate(leapDate).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Date, Is.EqualTo(leapDate));
        }

        [TestCase("Dark Star")]
        [TestCase("Cheese")]
        [TestCase("Northern Lights")]
        public async Task Post_GivenJarWithRealWorldDescription_ShouldCreateJar(string description)
        {
            //given
            var expectedJar = new JarBuilder().WithDescription(description).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Description, Is.EqualTo(description));
        }

        [Test]
        public async Task Post_GivenJarWithLowestId_ShouldCreateJar()
        {
            //given
            var id = Guid.Parse("00000000-0000-0000-0000-000000000000").ToString();
            var expectedJar = new JarBuilder().WithId(id).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Id, Is.EqualTo(id));
        }

        [Test]
        public async Task Post_GivenJarWithHighestId_ShouldCreateJar()
        {
            //given
            var id = Guid.Parse("ffffffff-ffff-ffff-ffff-ffffffffffff").ToString();
            var expectedJar = new JarBuilder().WithId(id).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Id, Is.EqualTo(expectedJar.Id));
        }

        [Test]
        public async Task Post_GivenJarWithRandomId_ShouldCreateJar()
        {
            //given
            var id = Guid.NewGuid().ToString();
            var expectedJar = new JarBuilder().WithId(id).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Id, Is.EqualTo(expectedJar.Id));
        }

        [TestCase("")]
        [TestCase("")]
        [TestCase("")]
        public async Task Post_GivenJarWithRealWorldNotes_ShouldCreateJar(string note)
        {
            //given
            var expectedJar = new JarBuilder().WithNote(note).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Note, Is.EqualTo(note));
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(64)]
        public async Task Post_GivenJarWithlog_ShouldCreateJar(int numberOfLogs)
        {
            //given
            var logs = Enumerable.Range(1, numberOfLogs).Select(x => new LogBuilder().Build()).ToList();
            var expectedJar = new JarBuilder().WithLogs(logs).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Logs.Count(), Is.EqualTo(numberOfLogs));
        }

        [Test]
        public async Task Post_GivenJarLogWithLogOfPastDate_ShouldCreateJar()
        {
            //given
            var pastDate = DateTime.Now.AddYears(-1);
            var expectedLog = new LogBuilder().WithDate(pastDate).Build();
            var expectedJar = new JarBuilder().WithLog(expectedLog).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Logs.First().Date, Is.EqualTo(pastDate));
        }

        [Test]
        public async Task Post_GivenJarLogWithPresentDate_ShouldCreateJarLog()
        {
            //given
            var presentDate = DateTime.Now;
            var expectedLog = new LogBuilder().WithDate(presentDate).Build();
            var expectedJar = new JarBuilder().WithLog(expectedLog).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Logs.First().Date, Is.EqualTo(presentDate));
        }

        [Test]
        public async Task Post_GivenJarLogWithFutureDate_ShouldCreateJarLog()
        {
            //given
            var futureDate = DateTime.Now.AddDays(1);
            var expectedLog = new LogBuilder().WithDate(futureDate).Build();
            var expectedJar = new JarBuilder().WithLog(expectedLog).Build();

            //executed
            var postResponse = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Logs.First().Date, Is.EqualTo(futureDate));
        }

        [Test]
        public async Task Post_GivenJarExsistAndNewLogAddded_ShouldUpdateJar()
        {
            //given
            var expectedJar = new JarBuilder().Build();
            var postResponse = await _jarLogApiRepository.Post(expectedJar);
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var log = new LogBuilder().WithNote("I was Added").Build();
            expectedJar.Logs.Add(log);

            //execute
            var postResponse2 = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse2.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Logs.Last().Note, Is.EqualTo(log.Note));
        }

        [Test]
        public async Task Post_GivenJarExistAndLogRemoved_ShouldUpdateJar()
        {
            //given
            var log = new LogBuilder().Build();
            var expectedJar = new JarBuilder().WithLog(log).Build();
            var postResponse = await _jarLogApiRepository.Post(expectedJar);
            Assert.That(postResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            expectedJar.Logs.Remove(log);

            //execute
            var postResponse2 = await _jarLogApiRepository.Post(expectedJar);

            //Should
            Assert.That(postResponse2.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var getResponse = await _jarLogApiRepository.Get(expectedJar.Id);
            Assert.That(getResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var jar = JsonConvert.DeserializeObject<Jar>(await getResponse.Content.ReadAsStringAsync());
            Assert.That(jar.Logs.Count(), Is.Zero);
        }
    }
}
