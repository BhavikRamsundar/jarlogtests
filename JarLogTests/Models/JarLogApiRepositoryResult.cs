﻿using System.Net;

namespace JarLogTests.Models
{
    public class JarLogApiRepositoryResult<T> : JarLogApiRepositoryResult
    {
        public T Content { get; set; }
    }

    public class JarLogApiRepositoryResult
    {
        public HttpStatusCode StatusCode;
    }
}