﻿using System;
using Newtonsoft.Json;

namespace JarLogTests.Models
{
    public class Log
    {

        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("note")]
        public string Note { get; set; }
    }
}