﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace JarLogTests.Models
{
    public class Jar
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("note")]
        public string Note { get; set; }
        [JsonProperty("logs")]
        public List<Log> Logs { get; set; }
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }

    }
}