﻿using System;
using System.Collections.Generic;
using System.Linq;
using JarLogTests.Models;

namespace JarLogTests.Builders
{
    public class JarBuilder
    {
        private DateTime _date = DateTime.Now;
        private string _description = "testDescription";
        private string _id = Guid.NewGuid().ToString();
        private List<Log> _logs = Enumerable.Range(1, 3).Select(x => new LogBuilder().Build()).ToList();
        private string _note = "testNote";

        public Jar Build()
        {
            var jar = new Jar
            {
                Date = _date,
                Description = _description,
                Id = _id,
                Logs = _logs,
                Note = _note
            };

            return jar;
        }

        public JarBuilder WithId(string id)
        {
            _id = id;

            return this;
        }

        public JarBuilder WithDate(DateTime date)
        {
            _date = date;

            return this;
        }

        public JarBuilder WithDescription(string description)
        {
            _description = description;

            return this;
        }

        public JarBuilder WithLogs(List<Log> logs)
        {
            _logs = logs;

            return this;
        }

        public JarBuilder WithLog(Log log)
        {
            _logs = new List<Log> { log };

            return this;
        }

        public JarBuilder WithNote(string note)
        {
            _note = note;

            return this;
        }
    }
}