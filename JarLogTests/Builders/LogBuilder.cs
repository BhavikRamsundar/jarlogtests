﻿using System;
using JarLogTests.Models;

namespace JarLogTests.Builders
{
    public class LogBuilder  
    {
        private DateTime _date = DateTime.Now;
        private string _note = "testNote";

        public Log Build()
        {
            var log = new Log
            {
                Date = _date,
                Note = _note
            };

            return log;
        }

        public LogBuilder WithDate(DateTime date)
        {
            _date = date;

            return this;
        }

        public LogBuilder WithNote(string note)
        {
            _note = note;

            return this;
        }
    }
}