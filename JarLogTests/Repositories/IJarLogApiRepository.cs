﻿using System.Net.Http;
using System.Threading.Tasks;
using JarLogTests.Models;

namespace JarLogTests.Repositories
{
    public interface IJarLogApiRepository
    {
        Task<HttpResponseMessage> Post(Jar jar);
        Task<HttpResponseMessage> Get(string id);
        Task<HttpResponseMessage> Delete(string id);
    }
}