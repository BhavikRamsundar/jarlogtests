﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using JarLogTests.Models;
using Newtonsoft.Json;

namespace JarLogTests.Repositories
{
    public class JarLogApiRepository : IJarLogApiRepository
    {
        private readonly Uri _baseUri;
        private HttpClient _client;

        public JarLogApiRepository(Uri baseUri)
        {
            _baseUri = baseUri;
            _client = new HttpClient();
        }

        public async Task<HttpResponseMessage> Post(Jar jar)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(_baseUri, "jars"));
            request.Content = new StringContent(JsonConvert.SerializeObject(jar), Encoding.UTF8, "application/json");

            return await _client.SendAsync(request);
        }

        public async Task<HttpResponseMessage> Get(string id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(_baseUri, $"jars/{id}"));

            return await _client.SendAsync(request);
        }

        public async Task<HttpResponseMessage> Delete(string id)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, new Uri(_baseUri, $"jars/{id}"));

            return await _client.SendAsync(request);
        }
    }
}